package fr.umfds.courriel;

public class Courriel {
    private String email = null;
    private String title = null;
    private String message = null;
    private String[] joints = null;
    private String[] motCle = new String[]{"PJ", "joint", "jointe"};

    public String[] getJoints() {
        return joints;
    }
    public void setJoints(String[] joints) {
        this.joints = joints;
    }
    public String getMessage() {
        return message;
    }
    public void setMessage(String message){
        this.message = message;
    }
    public String getTitle() {
        return title;
    }
    public void setTitle(String title){
        this.title = title;
    }
    public String getEmail() {
        return email;
    }
    public void setEmail(String email){
        this.email = email;  
    }

    Courriel(String _email,String _title,String _message,String[] _joints){
        this.email = _email;
        this.title = _title;
        this.message = _message;
        this.joints = _joints;
    }
    Courriel(String _email,String _title,String _message){
        this.email = _email;
        this.title = _title;
        this.message = _message;
    }
    public void envoyer() throws InvalidCourrielExcepetion{
        if (email == null )
            throw new InvalidCourrielExcepetion("ERROR PLEASE ENTER AN EMAIL ADRESS");
        else
            if(!email.matches("^[a-zA-Z][a-zA-Z0-9]*@[a-zA-Z]+[.][a-zA-Z]+$"))
                throw new InvalidCourrielExcepetion("ERROR "+email+" < IS NOT A VALID EMAIL ADRESS");
        
        if (title==null || title=="") throw new InvalidCourrielExcepetion("ERROR PLEASE ENTER A TITLE");
        
        
        for(String word:motCle){
            if(message.contains(word) && !(joints!=null  && !(joints.length==0)))throw new InvalidCourrielExcepetion("NO JOINTED FILES");
        }

        //TODO ENVOYER LE MESSAGE
    }

}
