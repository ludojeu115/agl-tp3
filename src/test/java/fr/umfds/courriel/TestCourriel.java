package fr.umfds.courriel;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;
class TestCourriel{
    Courriel c1;
    Courriel c2;
    Courriel c3;
    Courriel c4;
    Courriel c5;
    Courriel c6;
    Courriel c7;
    Courriel c8;
    @BeforeEach
    public void setUP(){
        c1= new Courriel("ludovic@lonlas.fr","titre","ceci est le message PJ",new String[]{"piecejointe1"});
        c2= new Courriel("ludovic@lonlasfr","","ceci est le message PJ");
        c3= new Courriel("ludoviclonlas.fr","titre","ceci est le message PJ",new String[]{"piecejointe1"});
        c4= new Courriel("ludovic@lonlasfr","titre","ceci est le message PJ",new String[]{"piecejointe1"});
        c5= new Courriel("ludovic@lonlas.fr","","ceci est le message PJ",new String[]{"piecejointe1"});
        c6= new Courriel("ludovic@lonlas.fr","titre","ceci est le message PJ");
        c7= new Courriel("ludovic@lonlas.fr","titre","ceci est le message PJ",new String[]{});
        c8= new Courriel("ludovic@lonlas.fr","title","ceci est le message ");
    }
    @Test
    public void ToutJuste() throws InvalidCourrielExcepetion{
        c1.envoyer();
    }
    @Test
    public void ToutJuste2() throws InvalidCourrielExcepetion{
        c8.envoyer();
    }
    @Test
    public void ToutFaux() throws InvalidCourrielExcepetion{
        assertThrows(InvalidCourrielExcepetion.class, ()->{c2.envoyer();});
    }
    @Test
    public void EmailArobaseManquant() throws InvalidCourrielExcepetion{
        assertThrows(InvalidCourrielExcepetion.class, ()->{c3.envoyer();});

    }
    @Test
    public void EmailPointManquant() throws InvalidCourrielExcepetion{
        assertThrows(InvalidCourrielExcepetion.class, ()->{c4.envoyer();});

    }
    @Test
    public void TitreManquant() throws InvalidCourrielExcepetion{
        assertThrows(InvalidCourrielExcepetion.class, ()->{c5.envoyer();});

    }
    @Test
    public void NoPJ() throws InvalidCourrielExcepetion{
        assertThrows(InvalidCourrielExcepetion.class, ()->{c6.envoyer();});

    }
    @Test
    public void EmptyPJ() throws InvalidCourrielExcepetion{
        assertThrows(InvalidCourrielExcepetion.class, ()->{c7.envoyer();});

    }
    public static Stream<Arguments> SourceEmailString(){
        return Stream.of( 
            Arguments.of("ludovic@lonlâs.fr","NOT A VALID EMAIL ADRESS"),
            Arguments.of("&as@fs.fr","NOT A VALID EMAIL ADRESS"),
            Arguments.of("ludovic@lonlas.'t","NOT A VALID EMAIL ADRESS"),
            Arguments.of("@lonlas.fr","NOT A VALID EMAIL ADRESS"),
            Arguments.of("ludovic@.fr","NOT A VALID EMAIL ADRESS"),
            Arguments.of("ludovic@lonlas.","NOT A VALID EMAIL ADRESS")
        );
    }
    @ParameterizedTest
    @ValueSource(strings = {"ludovic@lonlâs.fr", "&as@fs.fr", "ludovic@lonlas.'t", "@lonlas.fr", "ludovic@.fr", "ludovic@lonlas."}) // six numbers    
    public void EmailInvalide(String adress){
        Courriel c= new Courriel(adress,"titre","ceci est le message PJ",new String[]{"piecejointe1"});
        assertThrows(InvalidCourrielExcepetion.class,()->{c.envoyer();});
    }
    @ParameterizedTest(name = "RightException for Email")
    @MethodSource("SourceEmailString")
    public void EmailsInvalideVerif(String adress, String reason){
        Courriel c= new Courriel(adress,"titre","ceci est le message PJ",new String[]{"piecejointe1"});
        InvalidCourrielExcepetion ex =assertThrows(InvalidCourrielExcepetion.class,()->{c.envoyer();});
        assertTrue(ex.getMessage().contains(reason));
        
    }
}