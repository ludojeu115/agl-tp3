package fr.umfds.poste;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

class TestJUnit5{
    Lettre lettre1;
    Lettre lettre2;
    Colis colis1;
    @BeforeEach
    public void setUp(){
        lettre1 = new Lettre("Le pere Noel",
            "famille Kirik, igloo 5, banquise nord",
            "7877", 25, 0.00018f, Recommandation.un, false);
        lettre2 = new Lettre("Le pere Noel",
            "famille Kouk, igloo 2, banquise nord",
            "5854", 18, 0.00018f, Recommandation.deux, true);
        colis1 = new Colis("Le pere Noel", 
            "famille Kaya, igloo 10, terres ouest",
            "7877", 1024, 0.02f, Recommandation.deux, "train electrique", 200);      

    }
    @Nested
    class toString{
        @Test
        public void toString1(){
            assertTrue(colis1.toString().equals("Colis 7877/famille Kaya, igloo 10, terres ouest/2/0.02/200.0"));

        } 
        @Test
        public void toString2(){
            assertTrue(lettre1.toString().equals("Lettre 7877/famille Kirik, igloo 5, banquise nord/1/ordinaire"));

        } 
        @Test
        public void toString3(){
            assertTrue(lettre2.toString().equals("Lettre 5854/famille Kouk, igloo 2, banquise nord/2/urgence"));

        } 
    }
    @Nested
    class affranchissement{
        private float tolerancePrix=0.001f;
        @Test
        public void affr1(){
            assertTrue(Math.abs(lettre1.tarifAffranchissement()-1.0f)<tolerancePrix);
        }
        @Test
        public void affr2(){
            assertTrue(Math.abs(lettre2.tarifAffranchissement()-2.3f)<tolerancePrix);
        }
        @Test
        public void affr3(){
            assertTrue(Math.abs(colis1.tarifAffranchissement()-3.5f)<tolerancePrix);
        }
    }
    @Nested
    class Remboursement{
        private float tolerancePrix=0.001f;
        @Test
        public void remb1(){
            assertTrue(Math.abs(lettre1.tarifRemboursement()-1.5f)<tolerancePrix);
        }
        @Test
        public void remb2(){
            assertTrue(Math.abs(lettre2.tarifRemboursement()-15.0f)<tolerancePrix);
        }
        @Test
        public void remb3(){
            assertTrue(Math.abs(colis1.tarifRemboursement()-100.0f)<tolerancePrix);
        }
    }
    @Nested
    class ColisEXpress{
        ColisExpress colisEX1;
        ColisExpress colisEX2;
        @BeforeEach
        public void SetUP() throws ColisExpressInvalide{
            colisEX1 = new ColisExpress("Le pere Noel", 
            "famille Kaya, igloo 10, terres ouest",
            "7877", 24, 0.02f, Recommandation.deux, "train electrique", 200,true);     
            colisEX2 = new ColisExpress("Le pere Noel", 
            "famille Kaya, igloo 10, terres ouest",
            "7877", 24, 0.02f, Recommandation.deux, "train electrique", 200,false);             
        }
        @SuppressWarnings("unused")
		@Test
        public void creationBelowPDS() throws ColisExpressInvalide{
            ColisExpress colisTest = new ColisExpress("Le pere Noel", 
            "famille Kaya, igloo 10, terres ouest",
            "7877", 20, 0.02f, Recommandation.deux, "train electrique", 200,false); 

        }
        @SuppressWarnings("unused")
		@Test
        public void creationAbovePDS() throws ColisExpressInvalide{
            assertThrows(ColisExpressInvalide.class,()->{
            ColisExpress colisTest = new ColisExpress("Le pere Noel", 
            "famille Kaya, igloo 10, terres ouest",
            "7877", 1024, 0.02f, Recommandation.deux, "train electrique", 200,false); 
            });

        }
        @Test
        public void TestToString() throws ColisExpressInvalide{
            ColisExpress colisTest = new ColisExpress("Le pere Noel", 
            "famille Kaya, igloo 10, terres ouest",
            "7877", 20, 0.02f, Recommandation.deux, "train electrique", 200,false);
            assertTrue(colisTest.toString().equals("Colis express 7877/famille Kaya, igloo 10, terres ouest/2/0.02/200.0/20.0/2"));
        }
    }
}